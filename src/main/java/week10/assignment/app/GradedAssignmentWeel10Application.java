package week10.assignment.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradedAssignmentWeel10Application {

	public static void main(String[] args) {
		SpringApplication.run(GradedAssignmentWeel10Application.class, args);
		System.out.println("Hello Dev-Ops");
	}

}
